public class Almacenador{

    private int numeroUniforme;
    private int minutosJugados; 
    private String nombre;
    private String posicion;
    
    public Almacenador(int numeroUniforme, int minutosJugados, String nombre, String posicion){
    this.numeroUniforme = numeroUniforme;
    this.minutosJugados = minutosJugados;
    this.nombre = nombre;
    this.posicion = posicion;
    }
    
    
//--------Metodos get----------
    public int getNumeroUniforme(){
    return numeroUniforme;
    }
    
    public int getMinutosJugados(){
      return minutosJugados;
    }
        
    public String getNombre(){
    return nombre;
    }
    
    public String getPosicion(){
    return posicion;
    }
    
//--------Metodos set----------
    public void setNumeroUniforme(int numeroUniforme){
      this.numeroUniforme = numeroUniforme;
    }
    
    public void setMinutosJugados(int minutosJugados){
      this.minutosJugados = minutosJugados;
    }    
    
    public void setNombre(String nombre){
      this.nombre = nombre;
    }
        
    public void setPosicion(String posicion) {
      this.posicion = posicion;
    }
}