public class estadisticasPortero extends Almacenador{
   public estadisticasPortero(int numeroUniforme, int minutosJugados, String nombre, String posicion){
      super(numeroUniforme, minutosJugados, nombre, posicion);
   }

    public void mostrar(){
        System.out.println("Numero del uniforme del jugador: " + getNumeroUniforme());
        System.out.println("Jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Minutos jugados: " + getMinutosJugados() + "''");               
    }
}