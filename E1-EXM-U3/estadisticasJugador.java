public class estadisticasJugador extends Almacenador{

   private int goles;
   public estadisticasJugador(int numeroUniforme, int minutosJugados, int goles, String nombre, String posicion){
      
    super(numeroUniforme, minutosJugados, nombre, posicion);
      this.goles = goles;
    }
    
    public int getGoles(){
      return goles;
    }
    
    public void setGoles(int goles){
      this.goles = goles;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme del jugador: " + getNumeroUniforme());
        System.out.println("Jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Minutos jugados: " + getMinutosJugados() + "''");
        System.out.println("Goles marcados: " + getGoles());               
    }
}