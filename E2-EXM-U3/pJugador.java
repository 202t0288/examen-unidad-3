public class pJugador extends Almacenador{
   
   private int errores;
   public pJugador(int numeroUniforme, int errores, String nombre, String posicion){
    super(numeroUniforme, nombre, posicion);
      this.errores = errores;
   }
    
    public int getErrores(){
      return errores;
    }
    
    public void setErrores(int errores){
      this.errores = errores;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme del jugador: " + getNumeroUniforme());
        System.out.println("Jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Errores cometidos: " + getErrores());              
    }
}