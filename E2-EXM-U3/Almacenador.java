public class Almacenador{
   
   private int numeroUniforme; 
   private String nombre;
   private String posicion;
    
   public Almacenador(int numeroUniforme, String nombre, String posicion){
      this.numeroUniforme = numeroUniforme;
      this.nombre = nombre;
      this.posicion = posicion;
   }
 
//------Metodos get------
   public int getNumeroUniforme(){
      return numeroUniforme;
   }
    
   public String getNombre(){
      return nombre;
   }
    
   public String getPosicion(){
      return posicion;
   }
    
//------Metodos set------
   public void setNumeroUniforme(int numeroUniforme){
      this.numeroUniforme = numeroUniforme;
   }
    
   public void setNombre(String nombre){
      this.nombre = nombre;
   }
        
   public void setPosicion(String posicion){
      this.posicion = posicion;
   }
}