public class estadisticasPitcher extends Almacenador{
   
   private int ponchados;
   public estadisticasPitcher(int numeroUniforme, int ponchados, String nombre, String posicion){
      super(numeroUniforme, nombre, posicion);
         this.ponchados = ponchados;
   }
    
   public int getPonchados(){
      return ponchados;
   }
    
   public void setPonchados(int ponchados){
      this.ponchados = ponchados;
   }
    
   public void mostrar(){
      System.out.println("Numero del uniforme del jugador: " + getNumeroUniforme());
      System.out.println("Jugador: " + getNombre());
      System.out.println("Posicion: " + getPosicion());
      System.out.println("Ponchados: " + getPonchados());              
    }
}