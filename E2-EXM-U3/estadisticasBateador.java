public class estadisticasBateador extends Almacenador{
   
   private int hits;
   public estadisticasBateador(int numeroUniforme, int hits, String nombre, String posicion){
      super(numeroUniforme, nombre, posicion);
         this.hits = hits;
   }
    
   public int getHits(){
      return hits;
   }
    
   public void setHits(int hits){
      this.hits = hits;
   }
    
   public void mostrar(){
      System.out.println("Numero del uniforme del jugador: " + getNumeroUniforme());
      System.out.println("Jugador: " + getNombre());
      System.out.println("Posicion: " + getPosicion());
      System.out.println("Hits: " + getHits());              
    }
}